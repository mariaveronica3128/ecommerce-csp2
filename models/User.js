//[SECTION] Dependencies and Modules
const mongoose = require('mongoose'); 

//[SECTION] Blueprint Schema
    const userSchema = new mongoose.Schema({
		firstName: {
		 type: String,
		 required: [true,  'First Name is Required']
		}, 
		lastName: {
			type: String,
			required: [true,  'Last Name is Required']
		}, 
		number:{
			type: String,
			required: [true, 'Number is Required']
		},
    	email: {
    		type: String,
    		required: [true, 'Email is Required']
    	},
    	password: {
    		type: String,
    		required: [true, 'Password in Required']
    	},
    	isAdmin: {
    		type: Boolean,  
    		default: false 
    	},
		orders: [
		{
			orderId: {
				type: String,
				required: [true, 'Order ID is Required']   				
		   },

			orderedOn: {
			   type: Date,
			   default: new Date()
		   },
			status: {
			   type: String,
			   default: 'Added'
		   }

		}
		]
    });


//[SECTION] Model 
  const User = mongoose.model('User', userSchema)
  module.exports = User;
