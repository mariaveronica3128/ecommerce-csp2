//[SECTION] Dependencies and Modules
const exp = require("express");
const controller = require('../controllers/products');
const auth = require('../auth')

const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router(); 

//[SECTION] [POST] Routes

route.post('/create',verify, verifyAdmin, (req, res) => { 
    let data = req.body; 
    controller.createProduct(data).then(outcome => {
            res.send(outcome); 
    });
});

//[SECTION] [GET] Routes

route.get('/allproducts', (req, res) =>{
    controller.getAllProducts().then(results => {
      res.send(results);
    })
  });

route.get('/:item', (req, res) =>{
    let productId = req.params.item;
    controller.getItem(productId).then(results => {
      res.send(results);
    })
  });

route.get('/', (req, res) =>{

    controller.getAllActiveProducts().then(results => {
      res.send(results);
    });
  });


route.put('/', verify, verifyAdmin, (req,res) => {
    let details= req.body;
  
    let cId = details.id
    let cName = details.name;
    let cDesc = details.description;
    let cPrice = details.price;
    
    if (cId !== '' && cName !== ''&& cDesc !== '' && cPrice !==''){
        controller.updatedProduct(details).then(results =>{
            res.send(results)
        })
    }else{
            res.send(`Some details is mising, please check again`)
    }
   
});

route.put('/archive', verify, verifyAdmin, (req,res) => {
  let productId = req.body.id;
  //console.log(courseId);

  controller.deactivateProduct(productId).then(result => {
      res.send(result);
  });
});


route.put('/reactivate',verify, verifyAdmin, (req,res) => {
  let productId = req.body.id;

  controller.reactivateProduct(productId).then(result => {
      res.send(result);
  });
});

route.delete('/id', verify, verifyAdmin,(req, res) => {
  let id = req.body.id;

  controller.deleteProduct(id).then(result => {
    res.send(result);
  })
});

module.exports = route; 
