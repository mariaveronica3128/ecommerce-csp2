//[SECTION] Dependencies and Modules
const auth = require('../auth')
const exp = require("express");
const controller = require('../controllers/orders');

// destructive verify form auth
const {verify} = auth;

//[SECTION] Routing Component
const route = exp.Router(); 

//[SECTION] POST
route.post("/createOrder", verify, controller.createOrder);


//[SECTION] GET


route.get('/allOrder', verify, (req, res) =>{
    controller.getAllOrder().then(results => {
      res.send(results);
    })
});

route.get("/myOrder", verify, controller.getmyOrder);

//[SECTION] Export Route System
module.exports = route; 

