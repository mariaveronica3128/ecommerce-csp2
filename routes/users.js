//[SECTION] Dependencies and Modules
const exp = require("express"); 
const controller = require('./../controllers/users.js');
const auth = require('../auth')

const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
  const route = exp.Router(); 

  
//[SECTION] Routes-[POST]
//Registration
  route.post('/register', (req, res) => {
  
  	// console.table(req.body);
  	let userDetails = req.body;  
    controller.createUser(userDetails).then(outcome => {
      res.send(outcome);
    });
  });


// Login
route.post('/login', controller.loginUser);

//[SECTION] Routes-[GET]
//Registered Users
route.get('/allregisteredusers', verify, verifyAdmin, (req, res) =>{
  controller.getAllRegisteredUser().then(results => {
    res.send(results);
  })
});

//[SECTION] Routes-[PUT]

// USER TO ADMIN
route.put('/setAdmin', verify, verifyAdmin, (req,res) => {
  let userId = req.body.id;
  //console.log(courseId);

  controller.setAdmin(userId).then(result => {
      res.send(result);
  });
});

//ADMIN TO USER
route.put('/setUser', verify, verifyAdmin, (req,res) => {
  let adminId = req.body.id;
  //console.log(courseId);

  controller.setUser(adminId).then(result => {
      res.send(result);
  });
});



//[SECTION] Routes-[DELETE]

//[SECTION] Expose Route System
  module.exports = route; 

