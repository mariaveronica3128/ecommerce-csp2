//[SECTION] Dependencies and Modules
const User = require('../models/User');
const auth = require('../auth');
const bcrypt = require("bcrypt");
const dotenv = require('dotenv').config();


//[SECTION] Environment Setup
const salt = parseInt(process.env.SALT);


//[SECTION] Functionality [CREATE]
//REGISTRATION
   module.exports.createUser = (info) => {
    //  right side dapat same sa postman sa body
    //postman to each var
     let fName = info.firstName;
     let lName = info.lastName;
     let Useremail = info.email;
     let Userpassword = info.password;
     let Usergender =info.gender;
     let mobileNum =info.number;

    // leftside galing models same name dapat 
    //iluluwas niya
     let newUser = new User({
        firstName: fName,
        lastName: lName,
        email: Useremail,
        password: bcrypt.hashSync(Userpassword,salt),
        gender: Usergender,
        number: mobileNum

     }) ;


        return User.findOne({email: Useremail}).then((userFound, error) => {
            if (userFound){
                return "Email exists. Use another email to register"
            }
            else{
                return newUser.save().then((createdUser, error) => {
            
                    if (error) {
                         
                        return 'Failed to create new user';
                    } else {
                        
                        return `Account ${createdUser.firstName} has been created` ; 
                    }
                });
            }
        });
    };

    
//LOGIN
module.exports.loginUser = (req, res) => {

    User.findOne({email: req.body.email}).then(userFound => {

      if(userFound === null){
        return res.send("User Not Found");

      } else {

        const isPasswordCorrect = bcrypt.compareSync(req.body.password, userFound.password)
        console.log(isPasswordCorrect)
        if(isPasswordCorrect){
          return res.send({accessToken: auth.createAccessToken(userFound)})
        } else {
          return res.send("Incorrect Password")
        }
      }
    })
    .catch(err => res.send(err))
};

//RETRIEVING ALL REGISTERED USER
    module.exports.getAllRegisteredUser = () =>{
    return User.find({}).then(listofRegisteredUsers =>{
        
        return listofRegisteredUsers;
    })
   };


// USER to ADMIN
  module.exports.setAdmin = (id) =>{
    let updates = {
    isAdmin: true
    }
    return User.findByIdAndUpdate(id,updates).then((setAdmin, error) =>{
        console.log(setAdmin)
        if (setAdmin === null){
        return 'Cannot find User';
        }
        else if (setAdmin.isAdmin){
            return 'Already an Admin';
        }
        else {
          return 'Successfully setting user as Admin';
        }
    })
  };

  
//ADMIN TO USER
module.exports.setUser = (id) =>{
  let updates = {
  isAdmin: false
  }
  return User.findByIdAndUpdate(id,updates).then((setUser, error) =>{
      console.log(setUser)
      if (setUser === null){
      return 'Cannot find admin';
      }
      else if (!setUser.isAdmin){
          return 'Already an regular user';
      }
      else {
        return 'Successfully setting Admin as regular user';
      }
  })
};