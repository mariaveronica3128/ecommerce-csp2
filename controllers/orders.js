const Order = require('../models/Order');
const User = require('../models/User');
const Product = require('../models/Product');
const dotenv = require("dotenv"); 
const req = require('express/lib/request');
const res = require('express/lib/response');

//CREATE ORDER
module.exports.createOrder = async (req,res) => {
    console.log(req.user.id)

    if (req.user.isAdmin) {
        return res.send ("Action Forbidden")
    }

    let prodId = req.body.productId;

        let isCreatedOrder = await Product.findById(prodId).then(foundProduct => {
        console.log(foundProduct)
        console.log(foundProduct.price)
            let newOrder = new Order({
                userId: req.user.id,
                orders: [{
                    productId: prodId,
                    quantity: req.body.quantity
                    }],
                totalAmount: req.body.quantity * foundProduct.price,    
            })
                
            return newOrder.save().then(foundProduct => true, globalThis.getOrderId = foundProduct.id)
                .catch(err => err.message )})

            if(isCreatedOrder !== true){
                return res.send({message: isCreatedOrder})
            }

            let isUserUpdated = await User.findByIdAndUpdate(req.user.id).then(userUpdate => {
                let customerOrder ={
                    orderId: getOrderId
                }
                
            userUpdate.orders.push(customerOrder);
            return userUpdate.save().then(user => true).catch(err => err.message)
            })

            if(isUserUpdated != true){
            return res.send({message: isUserUpdated})
            
            }

            if(isCreatedOrder && isUserUpdated){
                return res.send({message: "Ordered successfuly"})
            }
};


//GET ALL ORDERS

module.exports.getAllOrder= () =>{
    
    return Order.find({}).then(listofOrders =>{
        return listofOrders;
    });
};


//USER's ORDER
    module.exports.getmyOrder = (req,res) => {
    User.findById(req.user.id).then(result =>res.send(result))
        .catch(err => res.send(err))
};
    
