const Product = require('../models/Product');

module.exports.createProduct = (info) => {
    let pName = info.name;
    let pDesc = info.description;
    let pCost = info.price;

    let newProduct = new Product({
        name: pName,
        description: pDesc,
        price: pCost
    });

    return Product.findOne({name: pName}).then((productFound, error) => {
        if (productFound){
            return "Product Exist. Add new product"
        }
        else{
            return newProduct.save().then((createdProduct, error) => {
        
                if (error) {
                     
                    return 'Failed to add new product';
                } else {
                    
                    return `New Product ${createdProduct.name} has been created` ; 
                }
            });
        }
    });
};


//All PRODUCTS
    
module.exports.getAllProducts = () =>{
    
    return Product.find({}).then(listofProducts =>{
        return listofProducts;
    });
};

//SINGLE PRODUCT
module.exports.getItem = (id) => {
    return Product.findById(id).then(resultOfQuery => 
     {
         if(resultOfQuery){
            return resultOfQuery
         }
         else{
            return 'Product does not exist'
         }
     });
};

//ACTIVE PRODUCTS
module.exports.getAllActiveProducts = () =>{
    return Product.find({isActive: true}).then(resultOfQuery => {
       return resultOfQuery;
    });
};

//UPDATING A PRODUCT
module.exports.updatedProduct = (details) => {
    let cProductId = details.id;
    let cName = details.name;
    let cDesc = details.description;
    let cPrice = details.price;
 
    let updatedProduct = {
     id: cProductId,
     name: cName,
     description:cDesc,
     price: cPrice
    }
 
    return Product.findByIdAndUpdate(cProductId, updatedProduct).then((
       productUpdated, err)=>{
           console.log(productUpdated)
       if (productUpdated ===null){
        return 'Cannot find product';
       }
       else {
          return 'Sucessfully updated product';
       }
    })
};

//Archive A Product
module.exports.deactivateProduct = (id) =>{
    let updates = {
    isActive: false
    }
    return Product.findByIdAndUpdate(id,updates).then((archived, error) =>{
        console.log(archived)
        if (archived ===null){
         return 'Cannot find product';
        }
        else if (!archived.isActive){
            return 'Already archived';
        }
        else {
           return 'Successfully archived product';
        }
     })
 };

 //Reactivate A Product
 module.exports.reactivateProduct = (id) =>{
    let updates = {
          isActive: true
    }
    return Product.findByIdAndUpdate(id,updates).then((reactivate, error) => {
       if (reactivate === null) {
        return 'Cannot activate Product'
       }
       else if (reactivate.isActive){
        return 'Product has been activated already'
        }
       else {
        return `Sucessfully activated product`
       }
    });
 };

 //Delete A Product
 module.exports.deleteProduct= (id) => {
    return Product.findByIdAndRemove(id).then((
       removedProduct, err) => {
        if (removedProduct == null) {
        return 'No product was removed';
        }
        else if (!removedProduct){
        return 'Product has been removed already'
        }
        else{
        return 'A product is successfully deleted';
        };
    });
};
